class Slider {
	constructor(el) {
		this.el = document.getElementById(el);

		this.width = this.el.offsetWidth;
		this.length = this.slides().length;
		this.current = 0;

		Array.from(this.slides()).forEach(li => li.style.width = this.width + 'px');

		this.setSliderHeight();

		this.el.getElementsByClassName('prev')[0].onclick = (e) => this.prev(e);
		this.el.getElementsByClassName('next')[0].onclick = (e) => this.next(e);
	}

	slides() {
		return this.el.getElementsByTagName('li');
	}

	setSliderHeight() {
		this.el.getElementsByTagName('ul')[0].style.height = this.slides()[this.current].offsetHeight + 'px';
	}

	prev(e) {
		if (this.current === 0) {
			return;
		}

		this.current--;
		this.slide();
	}

	next(e) {
		if (this.current === this.length - 1) {
			return;
		}

		this.current++;
		this.slide();
	}

	slide() {
		Array.from(this.slides()).forEach(li => {
			let leftValue = -1 * this.current * this.width;
		    li.style.left = leftValue + 'px';
		});

		this.setSliderHeight();
	}
}


const sliderOne = new Slider('slider-one');